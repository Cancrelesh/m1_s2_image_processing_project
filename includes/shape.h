/* shape.h */
#ifndef __SHAPE__
#define __SHAPE__

#include <g3x.h>

typedef struct _shape_
{
    int n1, n2, n3;
    G3Xpoint *vrtx;
    G3Xvector *norm;

    void (*alloc_array)(struct _shape_ *);
    void (*fill_shape)(struct _shape_ *);
    void (*free_shape)(struct _shape_ *);

    void (*draw_points)(struct _shape_ *, G3Xvector scale_factor); /* mode GL_POINTS */
    void (*draw_faces)(struct _shape_ *, G3Xvector scale_factor);  /* mode GL_TRIANGLES */

} Shape;

#endif
